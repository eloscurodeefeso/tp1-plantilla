import static org.junit.Assert.*;

import org.junit.Test;

import elemento.Agua;
import elemento.Aire;
import elemento.Elemento;
import elemento.Fuego;
import elemento.Tierra;

public class ElementoTest {
	@Test
	public void aguaEsUnElemento() {
		Elemento elemento = new Agua();
		assertNotNull(elemento);
	}
	@Test
	public void tierraEsUnElemento() {
		Elemento elemento = new Tierra();
		assertNotNull(elemento);
	}
	@Test
	public void fuegoEsUnElemento() {
		Elemento elemento = new Fuego();
		assertNotNull(elemento);
	}
	@Test
	public void aireEsUnElemento() {
		Elemento elemento = new Aire();
		assertNotNull(elemento);
	}
	@Test
	public void aguaPoseeUnNombreYNoEsNuloNiVacio() {
		Elemento elemento = new Agua();
		String nombreElemento = elemento.getNombre();
		assertTrue(nombreElemento != null && nombreElemento.length() > 0);
	}
	@Test
	public void tierraPoseeUnNombreYNoEsNuloNiVacio() {
		Elemento elemento = new Tierra();
		String nombreElemento = elemento.getNombre();
		assertTrue(nombreElemento != null && nombreElemento.length() > 0);
	}
	@Test
	public void fuegoPoseeUnNombreYNoEsNuloNiVacio() {
		Elemento elemento = new Fuego();
		String nombreElemento = elemento.getNombre();
		assertTrue(nombreElemento != null && nombreElemento.length() > 0);
	}
	@Test
	public void airePoseeUnNombreYNoEsNuloNiVacio() {
		Elemento elemento = new Aire();
		String nombreElemento = elemento.getNombre();
		assertTrue(nombreElemento != null && nombreElemento.length() > 0);
	}
	@Test
	public void aguaSuperaEnAtaqueAlFuego() {
		Elemento elementoAtacante = new Agua();
		Elemento elementoDefensor = new Fuego();
		assertTrue(elementoAtacante.superaEnAtaqueAlElemento(elementoDefensor));
	}
	@Test
	public void aguaNoSuperaEnAtaqueALaTierra() {
		Elemento elementoAtacante = new Agua();
		Elemento elementoDefensor = new Tierra();
		assertFalse(elementoAtacante.superaEnAtaqueAlElemento(elementoDefensor));
	}
	@Test
	public void aguaNoSuperaEnAtaqueAlAire() {
		Elemento elementoAtacante = new Agua();
		Elemento elementoDefensor = new Aire();
		assertFalse(elementoAtacante.superaEnAtaqueAlElemento(elementoDefensor));
	}
	@Test
	public void aguaNoSuperaEnAtaqueAlAgua() {
		Elemento elementoAtacante = new Agua();
		Elemento elementoDefensor = new Agua();
		assertFalse(elementoAtacante.superaEnAtaqueAlElemento(elementoDefensor));
	}
	@Test
	public void fuegoNoSuperaEnAtaqueAlFuego() {
		Elemento elementoAtacante = new Fuego();
		Elemento elementoDefensor = new Fuego();
		assertFalse(elementoAtacante.superaEnAtaqueAlElemento(elementoDefensor));
	}
	@Test
	public void fuegoSuperaEnAtaqueALaTierra() {
		Elemento elementoAtacante = new Fuego();
		Elemento elementoDefensor = new Tierra();
		assertTrue(elementoAtacante.superaEnAtaqueAlElemento(elementoDefensor));
	}
	@Test
	public void fuegoNoSuperaEnAtaqueAlAire() {
		Elemento elementoAtacante = new Fuego();
		Elemento elementoDefensor = new Aire();
		assertFalse(elementoAtacante.superaEnAtaqueAlElemento(elementoDefensor));
	}
	@Test
	public void fuegoNoSuperaEnAtaqueAlAgua() {
		Elemento elementoAtacante = new Fuego();
		Elemento elementoDefensor = new Agua();
		assertFalse(elementoAtacante.superaEnAtaqueAlElemento(elementoDefensor));
	}
	@Test
	public void tierraNoSuperaEnAtaqueAlFuego() {
		Elemento elementoAtacante = new Tierra();
		Elemento elementoDefensor = new Fuego();
		assertFalse(elementoAtacante.superaEnAtaqueAlElemento(elementoDefensor));
	}
	@Test
	public void tierraNoSuperaEnAtaqueALaTierra() {
		Elemento elementoAtacante = new Tierra();
		Elemento elementoDefensor = new Tierra();
		assertFalse(elementoAtacante.superaEnAtaqueAlElemento(elementoDefensor));
	}
	@Test
	public void tierraSuperaEnAtaqueAlAire() {
		Elemento elementoAtacante = new Tierra();
		Elemento elementoDefensor = new Aire();
		assertTrue(elementoAtacante.superaEnAtaqueAlElemento(elementoDefensor));
	}
	@Test
	public void tierraNoSuperaEnAtaqueAlAgua() {
		Elemento elementoAtacante = new Tierra();
		Elemento elementoDefensor = new Agua();
		assertFalse(elementoAtacante.superaEnAtaqueAlElemento(elementoDefensor));
	}
	@Test
	public void aireNoSuperaEnAtaqueAlFuego() {
		Elemento elementoAtacante = new Aire();
		Elemento elementoDefensor = new Fuego();
		assertFalse(elementoAtacante.superaEnAtaqueAlElemento(elementoDefensor));
	}
	@Test
	public void aireNoSuperaEnAtaqueALaTierra() {
		Elemento elementoAtacante = new Aire();
		Elemento elementoDefensor = new Tierra();
		assertFalse(elementoAtacante.superaEnAtaqueAlElemento(elementoDefensor));
	}
	@Test
	public void aireNoSuperaEnAtaqueAlAire() {
		Elemento elementoAtacante = new Aire();
		Elemento elementoDefensor = new Aire();
		assertFalse(elementoAtacante.superaEnAtaqueAlElemento(elementoDefensor));
	}
	@Test
	public void aireSuperaEnAtaqueAlAgua() {
		Elemento elementoAtacante = new Aire();
		Elemento elementoDefensor = new Agua();
		assertTrue(elementoAtacante.superaEnAtaqueAlElemento(elementoDefensor));
	}
	
}

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import batalla.Ataque;
import batalla.AtaqueEspecial;
import batalla.ParametroNuloException;
import elemento.Agua;
import elemento.Aire;
import elemento.Elemento;
import elemento.Fuego;
import elemento.Tierra;

public class AtaqueEspecialTest {
	

	@Test
	public void sePuedeCrearUnAtaqueEspecialDeAgua() throws ParametroNuloException {
		new AtaqueEspecial(new Agua());
	}
	
	@Test
	public void sePuedeCrearUnAtaqueEspecialDeFuego() throws ParametroNuloException {
		new AtaqueEspecial(new Fuego());
	}

	@Test
	public void sePuedeCrearUnAtaqueEspecialDeAire() throws ParametroNuloException {
		new AtaqueEspecial(new Aire());
	}
	
	@Test
	public void sePuedeCrearUnAtaqueEspecialDeTierra() throws ParametroNuloException {
		new AtaqueEspecial(new Tierra());
	}
	
	@Test
	public void unAtaqueEspecialEsUnAtaque() throws ParametroNuloException {
		Ataque miAtaque = new AtaqueEspecial(new Agua());
	}

	@Test(expected=ParametroNuloException.class)
	public void noSePuedeCrearUnAtaqueEspecialNulo() throws ParametroNuloException {
		new AtaqueEspecial(null);
	}
	
	@Test
	public void verificarMatrizDeDaņo() throws ParametroNuloException {
		Elemento[] elementos = {new Agua(), new Fuego(), new Tierra(), new Aire()};
		float[][] daņosEsperados = {{15, 18, 12, 15},{12,15,18,15},{15,15,15,15},{18,12,15,15}};
		float[][] daņosCalculados = new float[4][4];
		for(int elementoAtaque = 0; elementoAtaque < 4; elementoAtaque++) {
			Ataque miAtaque = new AtaqueEspecial(elementos[elementoAtaque]);
			for(int elementoDefensa = 0; elementoDefensa < 4; elementoDefensa++) {
				daņosCalculados[elementoAtaque][elementoDefensa] 
						= miAtaque.calcularDaņo(new Elemento[] {elementos[elementoDefensa]});	
			}
		}
		assertTrue(Arrays.deepEquals(daņosEsperados, daņosCalculados));
	}
	
}

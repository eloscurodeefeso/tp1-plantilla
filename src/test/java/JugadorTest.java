import static org.junit.Assert.*;

import org.junit.Test;

import batalla.Jugador;
import batalla.Monstruo;
import batalla.ParametroNuloException;
import elemento.Agua;
import elemento.Aire;
import elemento.Elemento;
import elemento.Fuego;
import elemento.Tierra;

public class JugadorTest extends BatallaTestUtils {

	@Test
	public void verificarQueSePuedaCrearUnJugadorNataliaConAguaYFuego() throws Exception {
		Jugador miJugador = new Jugador("Natalia", new Monstruo(new Agua(), new Fuego()));
		assertNotNull(miJugador);
	}

	@Test
	public void verificarQueSePuedaCrearUnJugadorPedroConAireYTierra() throws Exception {
		Jugador miJugador = new Jugador("Pedro", new Monstruo(new Aire(), new Tierra()));
		assertNotNull(miJugador);
	}

	@Test
	public void verificarQueSePuedaCrearUnJugadorNataliaConAguaYFuegoYDevuelvaSuNombre() throws Exception {
		Jugador miJugador = new Jugador("Natalia", new Monstruo(new Agua(), new Fuego()));
		assertEquals("Natalia", miJugador.obtenerNombre());
	}

	@Test
	public void verificarQueSePuedaCrearUnJugadorPedroConAireYTierraYDevuelvaSuNombre() throws Exception {
		Jugador miJugador = new Jugador("Pedro", new Monstruo(new Aire(), new Tierra()));
		assertNotNull("Pedro", miJugador.obtenerNombre());
	}

	@Test
	public void verificarQueCreadoUnJugadorNataliaConAguaYFuegoDevuelvaUnMonstruo() throws Exception {
		Jugador miJugador = new Jugador("Natalia", new Monstruo(new Agua(), new Fuego()));
		assertNotNull(miJugador.obtenerMonstruo());
	}
	
	@Test
	public void verificarQueCreadoUnJugadorPedroConAireYTierraDevuelvaUnMonstruo() throws Exception  {
		Jugador miJugador = new Jugador("Pedro", new Monstruo(new Aire(), new Tierra()));
		assertNotNull(miJugador.obtenerMonstruo());
	}

	@Test
	public void verificarQueCreadoUnJugadorNataliaConAguaYFuegoLasDevuelvaComoOpcionesDeAtaque() throws Exception {
		Jugador miJugador = new Jugador("Natalia", new Monstruo(new Agua(), new Fuego()));
		assertTrue(contieneArrayArray(miJugador.obtenerMonstruo().generarOpciones(),
				new Elemento[] { new Agua(), new Fuego() }, 0));
	}
	
	@Test
	public void verificarQueCreadoUnJugadorPedroConAireYTierraLasDevuelvaComoOpcionesDeAtaque() throws Exception  {
		Jugador miJugador = new Jugador("Pedro", new Monstruo(new Aire(), new Tierra()));
		assertTrue(contieneArrayArray(miJugador.obtenerMonstruo().generarOpciones(),
				new Elemento[] { new Aire(), new Tierra() }, 0));
	}

	@Test(expected=Exception.class)
	public void noSePuedeCrearUnJugadorSinNombre() throws Exception  {
		new Jugador("", new Monstruo(new Agua(), new Fuego()));
	}

	@Test(expected=Exception.class)
	public void noSePuedeCrearUnJugadorConNombreNulo() throws Exception  {
		new Jugador(null, new Monstruo(new Agua(), new Fuego()));
	}

	@Test(expected=ParametroNuloException.class)
	public void noSePuedeCrearUnJugadorConMonstruoNulo() throws Exception  {
		new Jugador("Jose", null);
	}
	
}

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import batalla.Ataque;
import batalla.ParametroNuloException;
import elemento.Agua;
import elemento.Aire;
import elemento.Elemento;
import elemento.Fuego;
import elemento.Tierra;

public class AtaqueTest {

	@Test
	public void sePuedeCrearUnAtaqueDeAgua() throws ParametroNuloException {
		new Ataque(new Agua());
	}
	
	@Test
	public void sePuedeCrearUnAtaqueDeFuego() throws ParametroNuloException {
		new Ataque(new Fuego());
	}

	@Test
	public void sePuedeCrearUnAtaqueDeAire() throws ParametroNuloException {
		new Ataque(new Aire());
	}
	
	@Test
	public void sePuedeCrearUnAtaqueDeTierra() throws ParametroNuloException {
		new Ataque(new Tierra());
	}
	
	@Test(expected=Exception.class)
	public void noSePuedeCrearUnAtaqueNulo() throws ParametroNuloException {
		new Ataque(null);
	}
	
	@Test
	public void verificarMatrizDeDaņo() throws ParametroNuloException {
		Elemento[] elementos = {new Agua(), new Fuego(), new Tierra(), new Aire()};
		float[][] daņosEsperados = {{10, 12, 8, 10},{8,10,12,10},{10,10,10,10},{12,8,10,10}};
		float[][] daņosCalculados = new float[4][4];
		for(int elementoAtaque = 0; elementoAtaque < 4; elementoAtaque++) {
			Ataque miAtaque = new Ataque(elementos[elementoAtaque]);
			for(int elementoDefensa = 0; elementoDefensa < 4; elementoDefensa++) {
				daņosCalculados[elementoAtaque] [elementoDefensa]
						= miAtaque.calcularDaņo(new Elemento[] {elementos[elementoDefensa]});	
			}
		}
		assertTrue(Arrays.deepEquals(daņosEsperados, daņosCalculados));
	}
	
}

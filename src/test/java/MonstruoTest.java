import static org.junit.Assert.*;

import org.junit.Test;

import batalla.Ataque;
import batalla.Monstruo;
import batalla.ParametroNuloException;
import elemento.Agua;
import elemento.Aire;
import elemento.Elemento;
import elemento.Fuego;
import elemento.Tierra;


public class MonstruoTest {

	@Test
	public void sepuedeCrearUnMonstruoConDosElementosAguaYFuego() throws Exception {
		Monstruo miMonstruo = new Monstruo(new Agua(), new Fuego());
		assertNotNull(miMonstruo);
	}

	@Test
	public void sepuedeCrearUnMonstruoConDosElementosAireYTierra() throws  Exception {
		Monstruo miMonstruo = new Monstruo(new Aire(), new Tierra());
		assertNotNull(miMonstruo);
	}

	@Test
	public void sepuedeCrearUnMonstruoConUnElementoAguaYOtroNull() throws  Exception{
		Monstruo miMonstruo = new Monstruo(new Agua(), null);
		assertNotNull(miMonstruo);
	}

	@Test
	public void sepuedeCrearUnMonstruoConUnElementoNullYOtroTierra() throws Exception{
		Monstruo miMonstruo = new Monstruo(null, new Tierra());
		assertNotNull(miMonstruo);
	}

	@Test(expected=ParametroNuloException.class)
	public void noSePuedeCrearUnMonstruoSinElementos() throws  Exception  {
		new Monstruo(null, null);
	}
	
	@Test(expected=Exception.class)
	public void noSePuedeCrearUnMonstruoConElementosIguales() throws  Exception  {
		new Monstruo(new Agua(), new Agua());
	}

	@Test
	public void creadoUnMonstruoConDosElementosDevuelveLosMismosComoOpciones() throws  Exception{
		Elemento[] elementosElegidos = new Elemento[] { new Agua(), new Fuego() };
		Monstruo miMonstruo = new Monstruo(elementosElegidos[0], elementosElegidos[1]);
		assertArrayEquals(elementosElegidos, miMonstruo.generarOpciones());
	}

	@Test
	public void creadoUnMonstruoConDosElementosDevuelveCadaUnoComoOpciones() throws  Exception {
		Elemento[] elementosElegidos = new Elemento[] { new Agua(), new Fuego() };
		Monstruo miMonstruo = new Monstruo(elementosElegidos[0], elementosElegidos[1]);
		Elemento[] opcionesObtenidas = new Elemento[] { miMonstruo.generarOpciones(1), miMonstruo.generarOpciones(2) };
		assertArrayEquals(elementosElegidos, opcionesObtenidas);
	}

	@Test
	public void creadoUnMonstruoConUnElementoNoSePuedeGenerarUnAtaqueNulo() throws  Exception {
		Monstruo miMonstruo = new Monstruo(new Agua(), null);
		Ataque miAtaque = miMonstruo.generarAtaque(null);
		assertNull(miAtaque);
	}

	@Test
	public void creadoUnMonstruoConUnElementoPuedeGenerarUnAtaqueDeEseElemento() throws Exception {
		Monstruo miMonstruo = new Monstruo(new Agua(), null);
		Elemento[] opciones = miMonstruo.generarOpciones();
		Ataque miAtaque = miMonstruo.generarAtaque(opciones[0]);
		assertNotNull(miAtaque);
	}

	@Test
	public void creadoUnMonstruoConUnElementoNoSePuedeGenerarUnAtaqueDeOtroElemento() throws Exception {
		Monstruo miMonstruo = new Monstruo(new Agua(), null);
		Ataque miAtaque = miMonstruo.generarAtaque(new Fuego());
		assertNull(miAtaque);
	}

	@Test
	public void creadoUnMonstruoConUnElementoNoSePuedeGenerarUnAtaqueEspecialNulo() throws Exception {
		Monstruo miMonstruo = new Monstruo(new Agua(), null);
		Ataque miAtaque = miMonstruo.generarAtaqueEspecial(null);
		assertNull(miAtaque);
	}

	@Test
	public void creadoUnMonstruoConUnElementoPuedeGenerarUnAtaqueEspecialDeEseElemento() throws  Exception {
		Monstruo miMonstruo = new Monstruo(new Agua(), null);
		Elemento[] opciones = miMonstruo.generarOpciones();
		Ataque miAtaque = miMonstruo.generarAtaque(opciones[0]);
		assertNotNull(miAtaque);
	}

	@Test
	public void creadoUnMonstruoConUnElementoNoSePuedeGenerarUnAtaqueEspecialDeOtroElemento() throws  Exception {
		Monstruo miMonstruo = new Monstruo(new Agua(), null);
		Ataque miAtaque = miMonstruo.generarAtaqueEspecial(new Fuego());
		assertNull(miAtaque);
	}
	
	@Test
	public void creadoUnMonstruoConUnElementoAlPrincipioHayCuatroAtaquesEspeciales() throws  Exception {
		Monstruo miMonstruo = new Monstruo(new Agua(), null);
		assertEquals(4, miMonstruo.cuantosAtaqueEspecialesQuedan());
	}

	@Test
	public void despuesDeUnAtaqueEspecialLosAtaquesEspecialesSeReducenEnUno() throws  Exception {
		Monstruo miMonstruo = new Monstruo(new Agua(), null);
		Elemento[] opciones = miMonstruo.generarOpciones();
		int ataquesPrevios = miMonstruo.cuantosAtaqueEspecialesQuedan();
		miMonstruo.generarAtaqueEspecial(opciones[0]);
		assertEquals(ataquesPrevios - 1, miMonstruo.cuantosAtaqueEspecialesQuedan());
	}
	
	@Test
	public void creadoUnMonstruoConUnElementoNoSePuedenGenerarMasDeCuatroAtaqueEspeciales() throws  Exception{
		Monstruo miMonstruo = new Monstruo(new Agua(), null);
		for(int i = 0; i < 4; i++) {
			miMonstruo.generarAtaqueEspecial(new Agua());		
		}
		Ataque miAtaque = miMonstruo.generarAtaqueEspecial(new Agua());
		assertNull(miAtaque);
	}
	
	@Test
	public void elEstadoVitalDeUnMonstruoRecienCreadoEs100() throws  Exception {
		Monstruo miMonstruo = new Monstruo(new Agua(), null);
		assertEquals(100, miMonstruo.obtenerEstadoVital(), 0);
	}

	@Test
	public void recibirUnAtaqueNuloNoTieneConsecuenciasEnElEstadoVital() throws  Exception {
		Monstruo miMonstruo = new Monstruo(new Fuego(), null);
		float estadoVitalPrevio = miMonstruo.obtenerEstadoVital();
		miMonstruo.recibirAtaque(null);
		assertEquals(estadoVitalPrevio, miMonstruo.obtenerEstadoVital(), 0);
	}

	@Test
	public void recibirUnAtaqueNuloNoTieneConsecuenciasEnLosElementos() throws Exception {
		Monstruo miMonstruo = new Monstruo(new Fuego(), null);
		Elemento[] opcionesPrevias = miMonstruo.generarOpciones();
		miMonstruo.recibirAtaque(null);
		assertArrayEquals(opcionesPrevias, miMonstruo.generarOpciones());
	}

	@Test
	public void elEstadoVitalDeUnMonstruoDespuesDeUnAtaqueEsMenorOIgual() throws  Exception{
		Monstruo monstruoAtacante = new Monstruo(new Agua(), null);
		Monstruo monstruoDefensor = new Monstruo(new Fuego(), null);
		Elemento[] opciones = monstruoAtacante.generarOpciones();
		Ataque miAtaque = monstruoAtacante.generarAtaque(opciones[0]);
		float estadoVitalPrevio = monstruoDefensor.obtenerEstadoVital();
		monstruoDefensor.recibirAtaque(miAtaque);
		assertTrue(estadoVitalPrevio >= monstruoDefensor.obtenerEstadoVital());
	}

	@Test
	public void cuandoElPrimerAtaqueAUnMonstruoTieneUnDañoMenorACienEsLoQueLeQuitaASuEstadoVital() throws  Exception {
		Monstruo monstruoAtacante = new Monstruo(new Agua(), null);
		Monstruo monstruoDefensor = new Monstruo(new Fuego(), null);
		Elemento[] elementosAtacante = monstruoAtacante.generarOpciones();
		Elemento[] elementosDefensor = monstruoDefensor.generarOpciones();
		Ataque miAtaque = monstruoAtacante.generarAtaque(elementosAtacante[0]);
		float estadoVitalPrevio = monstruoDefensor.obtenerEstadoVital();
		float dañoDeAtaque = miAtaque.calcularDaño(elementosDefensor);
		monstruoDefensor.recibirAtaque(miAtaque);
		assertEquals(estadoVitalPrevio - dañoDeAtaque, monstruoDefensor.obtenerEstadoVital(), 0);
	}

	@Test
	public void elMinimoEstadoVitalEsCero() throws Exception {
		Monstruo monstruoAtacante = new Monstruo(new Agua(), null);
		Monstruo monstruoDefensor = new Monstruo(new Fuego(), null);
		Elemento[] elementosAtacante = monstruoAtacante.generarOpciones();
		Elemento[] elementosDefensor = monstruoDefensor.generarOpciones();
		Ataque miAtaque = monstruoAtacante.generarAtaque(elementosAtacante[0]);
		while (monstruoDefensor.obtenerEstadoVital() >= miAtaque.calcularDaño(elementosDefensor)) {
			monstruoDefensor.recibirAtaque(miAtaque);
			miAtaque = monstruoAtacante.generarAtaque(elementosAtacante[0]);
		}
		monstruoDefensor.recibirAtaque(miAtaque);
		assertEquals(0, monstruoDefensor.obtenerEstadoVital(), 0);
	}

}

import elemento.Elemento;

public class BatallaTestUtils {
	    // Auxiliares que podriamos evitar si los elementos fueran un Enum y usar
		// ArrayList.containsAll por eso aplicamos recursividad sin "syntactic sugar":
		// sin metodo de entrada que calcule parametros iniciales
		protected boolean contieneArrayElemento(Elemento[] array, Elemento unElemento, int indiceBase) {
			return indiceBase < array.length && (array[indiceBase].getClass().isInstance(unElemento)
					|| contieneArrayElemento(array, unElemento, indiceBase++));
		}

		protected boolean contieneArrayArray(Elemento[] contenedor, Elemento[] incluido, int indiceBase) {
			return indiceBase < incluido.length && (contieneArrayElemento(contenedor, incluido[indiceBase], 0)
					|| contieneArrayArray(contenedor, incluido, indiceBase++));
		}
}

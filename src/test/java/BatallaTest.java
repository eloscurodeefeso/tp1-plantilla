import static org.junit.Assert.*;

import org.junit.Test;

import batalla.Ataque;
import batalla.Batalla;
import batalla.Jugador;
import batalla.Monstruo;
import batalla.ParametroNuloException;
import elemento.Agua;
import elemento.Aire;
import elemento.Elemento;
import elemento.Fuego;
import elemento.Tierra;

public class BatallaTest {
	
	@Test
	public void sePuedeCrearUnaBatallaConUnJugadorDeAireYFuegoYOtroDeAireYTierra() throws Exception {
		Batalla miBatalla = new Batalla("Natalia", new Agua(), new Fuego(), "Pedro", new Aire(), new Tierra());
		assertNotNull(miBatalla);
	}

	@Test
	public void siSeCreaUnaBatallaConUnJugadorNataliaDeAguaYFuegoMasOtroJugadorEntoncesIniciaNatalia()
			throws Exception {
		Batalla miBatalla = new Batalla("Natalia", new Agua(), new Fuego(), "Pedro", new Aire(), new Tierra());
		assertEquals("Natalia", miBatalla.obtenerJugadorAtacante().obtenerNombre());
	}

	@Test
	public void siSeCreaUnaBatallaConUnJugadorNataliaDeAguaYFuegoMasOtroJugadorEnElPrimerTurnoTieneAtaqueEspecialFuego()
			throws Exception {
		Batalla miBatalla = new Batalla("Natalia", new Agua(), new Fuego(), "Pedro", new Aire(), new Tierra());
		Monstruo miMonstruo = miBatalla.obtenerJugadorAtacante().obtenerMonstruo();
		assertTrue((miMonstruo.generarOpciones(1) instanceof Fuego | miMonstruo.generarOpciones(2) instanceof Fuego)
				& miMonstruo.cuantosAtaqueEspecialesQuedan() > 0);
	}

	@Test
	public void siSeCreaUnaBatallaConUnJugadorNataliaDeAguaYFuegoMasOtroJugadorGeneraUnAtaqueEspecialDeFuego()
			throws Exception {
		Batalla miBatalla = new Batalla("Natalia", new Agua(), new Fuego(), "Pedro", new Aire(), new Tierra());
		Ataque miAtaque = miBatalla.obtenerJugadorAtacante().obtenerMonstruo().generarAtaqueEspecial(new Fuego());
		assertNotNull(miAtaque);
	}

	@Test
	public void siSeCreaUnaBatallaConUnJugadorNataliaDeAguaYFuegoYOtroDeAireYTierraDespuesDeAtaqueEspecialDeFuegoLaVidaRestanteEs82()
			throws Exception {
		Batalla miBatalla = new Batalla("Natalia", new Agua(), new Fuego(), "Pedro", new Aire(), new Tierra());
		Ataque miAtaque = miBatalla.obtenerJugadorAtacante().obtenerMonstruo().generarAtaqueEspecial(new Fuego());
		miBatalla.jugada(miAtaque);
		assertEquals(82, miBatalla.obtenerJugadorAtacante().obtenerMonstruo().obtenerEstadoVital(), 0.1);
	}

	@Test
	public void siSeCreaUnaBatallaConUnJugadorDeAguaYFuegoYOtroPEdroDeAireYTierraSiASuTurnoPedroAtacaConEspecialDeAireAlOtroLeResta85DeVida()
			throws Exception {
		Batalla miBatalla = new Batalla("Natalia", new Agua(), new Fuego(), "Pedro", new Aire(), new Tierra());
		Ataque ataqueNatalia = miBatalla.obtenerJugadorAtacante().obtenerMonstruo().generarAtaqueEspecial(new Fuego());
		miBatalla.jugada(ataqueNatalia);
		Ataque ataquePedro = miBatalla.obtenerJugadorAtacante().obtenerMonstruo().generarAtaqueEspecial(new Aire());
		miBatalla.jugada(ataquePedro);
		assertEquals(85, miBatalla.obtenerJugadorAtacante().obtenerMonstruo().obtenerEstadoVital(), 0.1);
	}

	@Test(expected=Exception.class)
	public void noSePuedeCrearUnaBatallaConNombreNuloDelPrimerJugador() throws Exception {
		new Batalla(null, new Agua(), null, "Pedro", new Aire(), null);
	}
	
	@Test(expected=Exception.class)
	public void noSePuedeCrearUnaBatallaConNombreNuloDelSegundoJugador() throws Exception {
		new Batalla("Natalia", new Agua(), null, null, new Aire(), null);
	}
	
	@Test(expected=Exception.class)
	public void noSePuedeCrearUnaBatallaConNombresNulosDeJugadores() throws Exception {
		new Batalla(null, new Agua(), null, null, new Aire(), null);
	}
	
	@Test(expected=Exception.class)
	public void noSePuedeCrearUnaBatallaConNombreVacioDelPrimerJugador() throws Exception {
		new Batalla("", new Agua(), null, "Pedro", new Aire(), null);
	}
	
	@Test(expected=Exception.class)
	public void noSePuedeCrearUnaBatallaConNombreVacioDelSegundoJugador() throws Exception {
		new Batalla("Natalia", new Agua(), null, "", new Aire(), null);
	}
	
	@Test(expected=Exception.class)
	public void noSePuedeCrearUnaBatallaConNombresVaciosDeJugadores() throws Exception {
		new Batalla("", new Agua(), null, "", new Aire(), null);
	}
	
	@Test(expected=Exception.class)
	public void noSePuedeCrearUnaBatallaConLosMismoNombresDeJugadores() throws Exception {
		new Batalla("Natalia", new Agua(), null, "Natalia", new Aire(), null);
	}

	@Test(expected=Exception.class)
	public void noSePuedeCrearUnaBatallaConElementosNulosDelPrimerJugador() throws Exception {
		new Batalla("Natalia", null, null, "Pedro", new Aire(), null);
	}

	@Test(expected=Exception.class)
	public void noSePuedeCrearUnaBatallaConElementosNulosDelSegundoJugador() throws Exception {
		new Batalla("Natalia", new Agua(), null, "Pedro", null, null);
	}

	@Test(expected=Exception.class)
	public void noSePuedeCrearUnaBatallaConElementosNulosDeJugadores() throws Exception {
		new Batalla("Natalia", null, null, "Pedro", null, null);
	}

	@Test(expected = ParametroNuloException.class)
	public void noSePuedeJugarUnAtaqueNulo() throws Exception {
		Batalla miBatalla = new Batalla("Natalia", new Agua(), null, "Pedro", new Aire(), null);
		miBatalla.jugada(null);
	}

	@Test
	public void despuesDeUnaJugadaAvanzaElTurno() throws Exception {
		Batalla miBatalla = new Batalla("Natalia", new Agua(), null, "Pedro", new Aire(), null);
		Jugador jugadorAtacante = miBatalla.obtenerJugadorAtacante();
		Monstruo monstruoAtacante = jugadorAtacante.obtenerMonstruo();
		miBatalla.jugada(monstruoAtacante.generarAtaque(monstruoAtacante.generarOpciones()[0]));
		assertTrue(jugadorAtacante == miBatalla.obtenerJugadorDefensor());
	}
	
	@Test
	public void unaVezTerminadaUnaBatallaElGanadorEsElDefensorPorqueAvanzaElTurno() throws Exception {
		Batalla miBatalla = new Batalla("Natalia", new Agua(), null, "Pedro", new Aire(), null);
		do {
			Monstruo monstruoAtacante = miBatalla.obtenerJugadorAtacante().obtenerMonstruo();
			miBatalla.jugada(monstruoAtacante.generarAtaque(monstruoAtacante.generarOpciones()[0]));
		} while (!miBatalla.termino());
		assertTrue(miBatalla.obtenerJugadorDefensor() == miBatalla.obtenerGanador());
	}

	@Test
	public void unaVezTerminadaUnaBatallaDespuesDeUnaJugadaSigueTerminada() throws Exception {
		Batalla miBatalla = new Batalla("Natalia", new Agua(), null, "Pedro", new Aire(), null);
		do {
			Monstruo monstruoAtacante = miBatalla.obtenerJugadorAtacante().obtenerMonstruo();
			miBatalla.jugada(monstruoAtacante.generarAtaque(monstruoAtacante.generarOpciones()[0]));
		} while (!miBatalla.termino());
		Jugador atacante = miBatalla.obtenerJugadorAtacante();
		Monstruo monstruoAtacante = atacante.obtenerMonstruo();
		miBatalla.jugada(monstruoAtacante.generarAtaque(monstruoAtacante.generarOpciones()[0]));
		assertTrue(miBatalla.termino());
	}

	@Test
	public void unaVezTerminadaUnaBatallaUnaNuevaJugadaNoVuelveACambiarElTurno() throws Exception {
		Batalla miBatalla = new Batalla("Natalia", new Agua(), null, "Pedro", new Aire(), null);
		do {
			Monstruo monstruoAtacante = miBatalla.obtenerJugadorAtacante().obtenerMonstruo();
			miBatalla.jugada(monstruoAtacante.generarAtaque(monstruoAtacante.generarOpciones()[0]));
		} while (!miBatalla.termino());
		Jugador atacante = miBatalla.obtenerJugadorAtacante();
		Monstruo monstruoAtacante = atacante.obtenerMonstruo();
		miBatalla.jugada(monstruoAtacante.generarAtaque(monstruoAtacante.generarOpciones()[0]));
		assertTrue(atacante == miBatalla.obtenerJugadorAtacante());
	}

	@Test
	public void unaVezTerminadaUnaBatallaUnaNuevaJugadaNoTieneEfectoEnLaVidaDeLosMonstruos() throws Exception {
		Batalla miBatalla = new Batalla("Natalia", new Agua(), null, "Pedro", new Aire(), null);
		do {
			Monstruo monstruoAtacante = miBatalla.obtenerJugadorAtacante().obtenerMonstruo();
			miBatalla.jugada(monstruoAtacante.generarAtaque(monstruoAtacante.generarOpciones()[0]));
		} while (!miBatalla.termino());
		Jugador atacante = miBatalla.obtenerJugadorAtacante(), defensor = miBatalla.obtenerJugadorDefensor();
		Monstruo monstruoAtacante = atacante.obtenerMonstruo(), monstruoDefensor = defensor.obtenerMonstruo();
		float vidaAtacate = monstruoAtacante.obtenerEstadoVital(), vidaDefensor = monstruoDefensor.obtenerEstadoVital();
		miBatalla.jugada(monstruoAtacante.generarAtaque(monstruoAtacante.generarOpciones()[0]));
		assertTrue(vidaAtacate == miBatalla.obtenerJugadorAtacante().obtenerMonstruo().obtenerEstadoVital()
				&& vidaDefensor == miBatalla.obtenerJugadorDefensor().obtenerMonstruo().obtenerEstadoVital());
	}

	@Test
	public void todasLasPosiblesBatallasDeJuguadoresConUnElementoDebenTerminarEnHasta20Jugadas() throws Exception {
		Elemento[] elementos = { new Agua(), new Fuego(), new Tierra(), new Aire() };
		boolean es20elLimiteDeJugadas = true;
		for (Elemento elementoAtacante : elementos) {
			for (Elemento elementoDefensor : elementos) {
				Batalla miBatalla = new Batalla("Natalia", elementoAtacante, null, "Pedro", elementoDefensor, null);
				for (int i = 0; i < 20; i++) {
					Monstruo monstruoAtacante = miBatalla.obtenerJugadorAtacante().obtenerMonstruo();
					miBatalla.jugada(monstruoAtacante.generarAtaque(monstruoAtacante.generarOpciones()[0]));
				}
				es20elLimiteDeJugadas &= miBatalla.termino();
			}
		}
		assertTrue(es20elLimiteDeJugadas);
	}
}

package batalla;
import elemento.Elemento;

public class Monstruo {

	private float estadoVital = 100;
	private int ataquesEspecialesRestantes = 4;
	private Elemento[] misElementos = new Elemento[2];

	public Monstruo(Elemento primerElemento, Elemento segundoElemento) throws Exception {
		if (primerElemento == null && segundoElemento == null) {
			throw new ParametroNuloException("no se puede crear un monstruo sin elementos");
		}
		if (primerElemento != null && segundoElemento != null && primerElemento.equals(segundoElemento)) {
			throw new Exception("no se puede crear un monstruo con dos elementos iguales");
		}
		misElementos[0] = primerElemento;
		misElementos[1] = segundoElemento;

	}

	public float obtenerEstadoVital() {
		return estadoVital;
	}

	public int cuantosAtaqueEspecialesQuedan() {
		return ataquesEspecialesRestantes;
	}

	// crea el ataque con los elementos
	// el java lo puse por que no me compilaba pero hay que sacarlo
	public Ataque generarAtaque(Elemento elementoDeAtaque) throws ParametroNuloException {
		if (elElementoEsValido(elementoDeAtaque)) {
			Ataque ataqueNormal = new Ataque(elementoDeAtaque);
			return ataqueNormal;
		} else
			return null;
	}

	// hay que agregar que no se pueda realizar si no quedan ataques pero yo supongo
	// que se limita en la interfaz eso
	public AtaqueEspecial generarAtaqueEspecial(Elemento elementoDeAtaque) throws ParametroNuloException {
		if (elElementoEsValido(elementoDeAtaque) && ataquesEspecialesRestantes > 0) {
			AtaqueEspecial ataqueEspecial = new AtaqueEspecial(elementoDeAtaque);
			ataquesEspecialesRestantes--;
			return ataqueEspecial;
		} else
			return null;
	}
	
	private boolean elElementoEsValido(Elemento elementoDeAtaque) {
		boolean elementoValido = false;
		if (elementoDeAtaque != null) {
			for (int i = 0; i < misElementos.length; i++) {
				if (misElementos[i] != null) {
					if (misElementos[i].getClass().isInstance(elementoDeAtaque))
						elementoValido = true;
				}
			}
		}

		return elementoValido;
	}

	// se elige un posicion del array y devuelve el elemento correspondiente
	public Elemento generarOpciones(int i) {
		return misElementos[i - 1];
	}

	// devuelve una copia del array de elementos
	public Elemento[] generarOpciones() {
		return misElementos.clone();
	}

	// recibe un ataque con los elementos ya cargados y hace los calculos
	public void recibirAtaque(Ataque ataqueRecibido) {
		if (ataqueRecibido != null) {
			if (estadoVital >= ataqueRecibido.calcularDaņo(misElementos.clone())) {
				estadoVital -= ataqueRecibido.calcularDaņo(misElementos.clone());
			} else
				estadoVital = 0;
		}

	}

}

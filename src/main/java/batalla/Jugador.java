package batalla;

public class Jugador {

	
	private Monstruo miMonstruo;
	private String nombreDeMiMonstruo;
	
	public Jugador(String nombreDelMonstruoJ1, Monstruo miMonstruo) throws ParametroNuloException {
		if(nombreDelMonstruoJ1 == null || nombreDelMonstruoJ1.equals("")) {
			throw new ParametroNuloException("Debe insertarle un nombre al jugador");
		}if(miMonstruo == null ) {
			throw new ParametroNuloException("Debe asignarle un Monstruo al jugador");
		}
		this.nombreDeMiMonstruo = nombreDelMonstruoJ1;
		this.miMonstruo = miMonstruo;
	}

	public String obtenerNombre() {
		return nombreDeMiMonstruo;
	}
	
	public Monstruo obtenerMonstruo() {
		return miMonstruo;
	}
}

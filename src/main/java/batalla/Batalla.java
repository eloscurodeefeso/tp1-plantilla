package batalla;
import elemento.Agua;
import elemento.Aire;
import elemento.Elemento;
import elemento.Fuego;
import elemento.Tierra;

public class Batalla {

	private Jugador atacante;
	private Jugador defensor;
	public static final Elemento[] ELEMENTOS = new Elemento[] { new Fuego(), new Tierra(), new Aire(), new Agua(), null };

	public Batalla(String nombrePrimerJugador, 
			Elemento primerElementoPrimerJugador, 
			Elemento segundoElementoPrimerJugador,
			String nombreSegundoJugador, 
			Elemento primerElementoSegundoJugador, 
			Elemento segundoElementoSegundoJugador) throws Exception {

		atacante = new Jugador(nombrePrimerJugador, 
				new Monstruo(primerElementoPrimerJugador, segundoElementoPrimerJugador));
		defensor = new Jugador(nombreSegundoJugador, 
				new Monstruo(primerElementoSegundoJugador, segundoElementoSegundoJugador));
		if(nombrePrimerJugador.equalsIgnoreCase(nombreSegundoJugador)) {
			throw new Exception("Los nombres de los jugadores no pueden ser iguales.");
		}
	}

	public Jugador obtenerJugadorAtacante() {
		return atacante;
	}

	public Jugador obtenerJugadorDefensor() {
		return defensor;
	}

	public boolean termino() {
		return (atacante.obtenerMonstruo().obtenerEstadoVital() <= 0
				|| defensor.obtenerMonstruo().obtenerEstadoVital() <= 0);
	}

	// devuelve el ganador.
	// si la partida no termino devuelve null OJO!
	// puse defensor segun la logica de que se invierte luego de atacar
	public Jugador obtenerGanador() {
		Jugador ganador = null;
		if (termino()) {
			ganador = defensor;
		}
		return ganador;
	}

	// hace el calculo de da�o y ademas intercambia los roles(atacante/defensor)
	public void jugada(Ataque ataque) throws ParametroNuloException {
		if(ataque == null) {
			throw new ParametroNuloException("no se puede jugar un ataque nulo");
		}
		if (!termino()) {
		defensor.obtenerMonstruo().recibirAtaque(ataque);
			Jugador auxiliar = atacante;
			atacante = defensor;
			defensor = auxiliar;
		}
	}
}

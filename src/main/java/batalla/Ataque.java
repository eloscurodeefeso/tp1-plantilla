package batalla;
import elemento.Elemento;

public class Ataque {

	protected int da�oBase = 10;
	private Elemento elementoAtacante;
	private boolean ventajaEnAtaque;
	private boolean ventajaEnDefensa;

	public Ataque(Elemento elementoDelAtaque) throws ParametroNuloException {
		if(elementoDelAtaque == null) {
			throw new ParametroNuloException("no se puede realizar un ataque sin elemento");
		}
		elementoAtacante = elementoDelAtaque;
	}

	public float calcularDa�o(Elemento[] elementosDefensor) {

		establecerVentajas(elementosDefensor);
		return da�oBase + calcularVentaja(ventajaEnAtaque) - calcularVentaja(ventajaEnDefensa);
	}

	private float calcularVentaja(boolean enVentaja) {
		return enVentaja ? da�oBase / 5 : 0;
	}

	private void establecerVentajas(Elemento[] elementosDefensor) {
		ventajaEnAtaque = false;
		ventajaEnDefensa = false;
		for (int i = 0; i < elementosDefensor.length; i++) {
			if (elementosDefensor[i] != null) {
				ventajaEnAtaque |= elementoAtacante.superaEnAtaqueAlElemento(elementosDefensor[i]);
				ventajaEnDefensa |= elementosDefensor[i].superaEnDefensaAlElemento(elementoAtacante);
			}

		}

	}

}

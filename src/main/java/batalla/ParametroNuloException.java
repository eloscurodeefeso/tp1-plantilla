package batalla;

public class ParametroNuloException extends Exception{

	 /**
	 * dato autocompletado para que deje de mostrar el icono de error
	 */
	private static final long serialVersionUID = 3893834639933006699L;

	ParametroNuloException(String detalles) {
		super(detalles);
		}

}

package elemento;
public class Aire extends Elemento{

	@Override
	public boolean superaEnAtaqueAlElemento(Elemento elementoEnemigo) {
		return elementoEnemigo.equals(new Agua());
	}

	@Override
	public boolean superaEnDefensaAlElemento(Elemento elementoEnemigo) {
		return elementoEnemigo.equals(new Tierra());
	}

	@Override
	public String getNombre() {
		
		return "Aire";
	}
	
	
	
	
}

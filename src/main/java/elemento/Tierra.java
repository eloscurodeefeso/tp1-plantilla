package elemento;
public class Tierra extends Elemento{

	@Override
	public boolean superaEnAtaqueAlElemento(Elemento elementoEnemigo) {
		return elementoEnemigo.equals(new Aire());
	}

	@Override
	public boolean superaEnDefensaAlElemento(Elemento elementoEnemigo) {
		return elementoEnemigo.equals(new Agua());
	}

	@Override
	public String getNombre() {
		
		return "Tierra";
	}
	
	
	
	
}
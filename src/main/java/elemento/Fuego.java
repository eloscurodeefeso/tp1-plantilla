package elemento;

public class Fuego extends Elemento{

	@Override
	public boolean superaEnAtaqueAlElemento(Elemento elementoEnemigo) {
		return elementoEnemigo.equals(new Tierra());
	}

	@Override
	public boolean superaEnDefensaAlElemento(Elemento elementoEnemigo) {
		return elementoEnemigo.equals(new Aire());
	}

	@Override
	public String getNombre() {
		
		return "Fuego";
	}
	
	
	
	
}

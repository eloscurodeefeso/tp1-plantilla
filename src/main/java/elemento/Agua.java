package elemento;

public class Agua extends Elemento{

	@Override
	public boolean superaEnAtaqueAlElemento(Elemento elementoEnemigo) {
		return elementoEnemigo.equals(new Fuego());
	}

	@Override
	public boolean superaEnDefensaAlElemento(Elemento elementoEnemigo) {
		return elementoEnemigo.equals(new Fuego());
	}

	@Override
	public String getNombre() {
		
		return "Agua" /* tiene ventaja contra Fuego y desventaja contra Tierra */;
	}
	
	
	
	
}
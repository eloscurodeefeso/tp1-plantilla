package elemento;

public abstract class Elemento {

	// seguimos el esquema de fortalezas

	public abstract boolean superaEnAtaqueAlElemento(Elemento elementoEnemigo);

	// Reglas de Ataque = Agua>Fuego>Tierra>Aire>Agua.....
	public abstract boolean superaEnDefensaAlElemento(Elemento elementoEnemigo);
	// Reglas de Defensa = Agua>Fuego>Aire>Tierra>Agua.....
	
	public abstract String getNombre();

	public boolean equals(Elemento e) {
		return this.getClass().isInstance(e);
	}
	
}

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import batalla.Ataque;
import batalla.Batalla;
import batalla.Jugador;
import batalla.Monstruo;
import batalla.ParametroNuloException;
import elemento.Elemento;
import menu.MenuDeAtaques;
import menu.MenuDeElementos;

public class App {

	private static final int CANTIDAD_DE_JUGADORES = 2;
	private static final int CANTIDAD_DE_ELEMENTOS_POR_JUGADORES = 2;

	public static void main(String[] args) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			Batalla batalla = crearBatalla(br);
			jugarPartida(batalla, br);
			mostrarResultado(batalla);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	// Metodo que se encarga de crear la batalla, es decir, los jugadores y sus
	// elementos.
	private static Batalla crearBatalla(BufferedReader br) {
		Batalla miBatalla;
		String[] nombresJugadores = new String[CANTIDAD_DE_JUGADORES];
		Elemento[][] opcionesElementos = new Elemento[nombresJugadores.length][CANTIDAD_DE_ELEMENTOS_POR_JUGADORES];
		try {
			for (int i = 0; i < nombresJugadores.length; i++) {
				nombresJugadores[i] = ingresaNombre(i + 1, br);
				MenuDeElementos menu = new MenuDeElementos(br);
				for (int j = 0; j < opcionesElementos[i].length; j++) {
					opcionesElementos[i][j] = menu.elegir();
				}
			}
			miBatalla = new Batalla(nombresJugadores[0], opcionesElementos[0][0], opcionesElementos[0][1],
					nombresJugadores[1], opcionesElementos[1][0], opcionesElementos[1][1]);
		} catch (Exception i) {
			System.out.println("Surgio un error. " + i.getMessage() + "\r\nReintentar.\r\n");
			return crearBatalla(br);
		}
		return miBatalla;
	}

	/*
	 * Permite jugar la partida mientras los jugadores sigan vivos, calcula da�o
	 * recibido y vida actual al jugador atacado. Muestra los resultados parciales
	 * luego de cada ronda.
	 */
	private static void jugarPartida(Batalla batalla, BufferedReader br) throws IOException, ParametroNuloException {
		int jugadas = 0;
		while (!batalla.termino()) {
			Jugador jugadorAtacante = batalla.obtenerJugadorAtacante();
			Jugador jugadorDefensor = batalla.obtenerJugadorDefensor();
			Monstruo monstruoDefensor = jugadorDefensor.obtenerMonstruo();
			System.out.println("Turno del Jugador " + jugadorAtacante.obtenerNombre() + "\r\n");
			Ataque ataque = (new MenuDeAtaques(jugadorAtacante, br)).elegir();
			System.out.println("Da�o causado a " + jugadorDefensor.obtenerNombre());
			System.out.println(ataque.calcularDa�o(monstruoDefensor.generarOpciones()) + "\r\n");
			batalla.jugada(ataque);
			System.out.println("Vida actual de " + jugadorDefensor.obtenerNombre());
			System.out.println(monstruoDefensor.obtenerEstadoVital() + "\r\n");
			jugadas++;
			if (hanJugadoTodosSuTurno(jugadas)) {
				mostrarResultadoParcial(batalla);
			}
		}
	}

	private static void mostrarResultadoParcial(Batalla batalla) {
		Jugador jugadorAtacante = batalla.obtenerJugadorAtacante();
		Jugador jugadorDefensor = batalla.obtenerJugadorDefensor();
		System.out.println("Resultados parciales" + "\r\n" + "Vida de " + jugadorAtacante.obtenerNombre() + ": "
				+ jugadorAtacante.obtenerMonstruo().obtenerEstadoVital() + "\r\n" + "Vida de "
				+ jugadorDefensor.obtenerNombre() + ": " + jugadorDefensor.obtenerMonstruo().obtenerEstadoVital()
				+ "\r\n");

	}

	// Imprime el perdedor y el ganador de la batalla.
	private static void mostrarResultado(Batalla batalla) {
		System.out.println("El jugador " + batalla.obtenerJugadorAtacante().obtenerNombre() + " ha muerto" + "\r\n"
				+ "El ganador es " + batalla.obtenerGanador().obtenerNombre());
	}
	
	private static boolean hanJugadoTodosSuTurno(int jugadas) {
		return jugadas % CANTIDAD_DE_JUGADORES == 0;
	}

	private static String ingresaNombre(int orden, BufferedReader br) throws IOException {
		String linea;
		do {
			System.out.println("Nombre Del Jugador " + orden + ": ");
			linea = br.readLine();
		} while (linea.isEmpty());
		return linea;
	}
}
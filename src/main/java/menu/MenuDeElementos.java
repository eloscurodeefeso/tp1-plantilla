package menu;
import java.io.BufferedReader;

import batalla.Batalla;
import elemento.Elemento;

public class MenuDeElementos {

	private Elemento ultimaOpcion = null;
	BufferedReader br;

	public MenuDeElementos(BufferedReader br) {
		this.br = br;
	}

	public Elemento elegir() {
		Menu<Elemento> miMenu = new Menu<Elemento>("Ingrese el/los elementos del monstruo que desea:", br);
		for (Elemento elemento : Batalla.ELEMENTOS) {
			if (ultimaOpcion != elemento) {
				miMenu.agregarOpcion(new MenuItem<Elemento>(
						elemento == null ? "Ningun elemento" : "Elemento del tipo " + elemento.getNombre(), elemento));
			}
		}

		return ultimaOpcion = miMenu.obtenerEleccion();
	}
}

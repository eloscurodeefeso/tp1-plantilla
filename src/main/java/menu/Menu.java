package menu;

import java.io.BufferedReader;
import java.util.Iterator;
import java.util.LinkedList;

public class Menu<TipoValor> {

	private String titulo;
	private BufferedReader entrada;
	private LinkedList<MenuItem<TipoValor>> opciones;

	public Menu(String titulo, BufferedReader br) {
		this.titulo = titulo;
		this.entrada = br;
		this.opciones = new LinkedList<MenuItem<TipoValor>>();
	}

	public void agregarOpcion(MenuItem<TipoValor> menuItem) {
		opciones.add(menuItem);
	}

	public TipoValor obtenerEleccion() {
		mostrarMenu();
		int eleccion = 0;
		try {
			boolean eleccionInvalida;
			do {
				eleccion = Integer.parseInt(entrada.readLine());
				if (eleccionInvalida = (eleccion < 1 || eleccion > opciones.size()))
					System.out.println("Debe elegir una opcion entre 1 y " + opciones.size());
			} while (eleccionInvalida);
		} catch (Exception e) {
			System.out.println("Ha ocurrido un problema: " + e.getMessage() + ".\r\nVuelva a intentarlo.");
			return obtenerEleccion();
		}
		return opciones.get(eleccion - 1).valor;
	}

	private void mostrarMenu() {
		System.out.println(titulo);
		Iterator<MenuItem<TipoValor>> iter = opciones.iterator();
		int numeroDeOpciones = 0;
		while (iter.hasNext()) {
			numeroDeOpciones++;
			MenuItem<TipoValor> opcion = iter.next();
			System.out.println(numeroDeOpciones + ". " + opcion.mensaje);
		}
	}

}

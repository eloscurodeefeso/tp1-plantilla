package menu;

public class MenuItem<T> {
	
	public String mensaje;
	public T valor;

	public MenuItem(String mensaje, T valor) {
		this.mensaje = mensaje;
		this.valor = valor;
	}

}

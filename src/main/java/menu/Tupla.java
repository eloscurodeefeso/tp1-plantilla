package menu;

public class Tupla<TipoFst, TipoSnd> {
	public final TipoFst fst;
	public final TipoSnd snd;
	public Tupla(TipoFst fst, TipoSnd snd) {
		this.fst = fst;
		this.snd = snd;
	}
}

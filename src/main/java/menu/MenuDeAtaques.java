package menu;
import java.io.BufferedReader;

import batalla.Ataque;
import batalla.Jugador;
import batalla.Monstruo;
import batalla.ParametroNuloException;
import elemento.Elemento;

public class MenuDeAtaques {

	Jugador atacante;
	private BufferedReader br;
	
	public MenuDeAtaques(Jugador atacante, BufferedReader br) {
		this.atacante = atacante;
		this.br = br;
	}
	public Ataque elegir()
			throws ParametroNuloException {
		Monstruo monstruoAtacante = atacante.obtenerMonstruo();
		Elemento[] opciones = monstruoAtacante.generarOpciones();
		Menu<Tupla<Elemento, Boolean>> miMenu = new Menu<Tupla<Elemento, Boolean>>("Ingrese el ataque que desea:", br);
		for(Elemento elemento : opciones) {
			if(elemento != null) {
				miMenu.agregarOpcion(new MenuItem<Tupla<Elemento, Boolean>>("Ataque Simple del tipo " + elemento.getNombre(), new Tupla<Elemento, Boolean>(elemento, false)));
				if(quedanAtaquesEspeciales(atacante)) {
					miMenu.agregarOpcion(new MenuItem<Tupla<Elemento, Boolean>>("Ataque Especial del tipo " + elemento.getNombre(), new Tupla<Elemento, Boolean>(elemento, true)));
				}
			}
		}
		Tupla<Elemento, Boolean> eleccion = miMenu.obtenerEleccion();
		return eleccion.snd == true ?
			monstruoAtacante.generarAtaqueEspecial(eleccion.fst) :
		    monstruoAtacante.generarAtaque(eleccion.fst);
	}

	// Metodo que devuelve si el jugador actual (luego llamado al menu) tiene
	// ataques especiales disponibles.
	private boolean quedanAtaquesEspeciales(Jugador atacante) {
		return atacante.obtenerMonstruo().cuantosAtaqueEspecialesQuedan() > 0;
	}

}

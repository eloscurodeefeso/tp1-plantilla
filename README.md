**Informe TP1 AYP2**

**Integrantes:**

- Matias Monti
- Alejandro Araneda
- Gustavo Velazquez


**Decisiones de diseño:**

- La rueda de daño y defensa no coincidian con el test propuesto en el tp por lo tanto decidimos darle prioridad a la rueda de daño ya que esta cubria todos los casos.
- El ingreso de datos erroneos es manejado por excepciones. 
- Usamos herencia en la clase Elemento y sus subclases Fuego,Agua,Aire y Tierra. Tambien en Ataque y AtaqueEspecial.
- En el uml no tuvimos en cuenta ciertos metodos que fuimos agregando a las clases para el funcionamiento de la batalla.
- En la interfaz se decidio abstraerla de cualquier informacion codificada del juego, la interfaz por lo tanto utiliza todas las herramientas que tiene a su alcance.

**Descripción de cada archivo:**

- Elementos : Clase abstracta que le impone a sus subclases tener ciertos metodos.

- Agua/Fuego/Tierra/Aire : Subclases que heredan de Elemento cada uno tiene ventaja en ataque  y desventaja contra derterminado elemento y su nombre correspondiente.

- Ataque: Esta clase permite manejar el ataque es decir; seleccionar el objetivo y saber el daño que causa .

- AtaqueEspecial: Hace los mismo que ataque(hereda sus metodos) pero su daño base es mayor.

- Batalla: Maneja informacion de los jugadores y sus monstruos, permite realizar jugadas(ataques por turnos),saber cuando la partida esta terminada y quien es el ganador.

- Jugador: Habra dos en cada partida, cada uno tiene asigando un Monstruo al que puede elegirle hasta dos Elementos y no menos de 1.

- Monstruo: Tiene estado vital, elementos, puede realizar infinitos ataques normales a otros monstruos y hasta 4 ataques especiales mientras el elemento exista y sea del monstruo.

- ParametroNuloException: Clase que hereda de Exception usada en casos que se ingrese un parametro vacio o nulo como por ejemplo en el nombre.

- App: Se encarga de la interfaz del juego, modulada en crear la batalla, jugar la partida y mostrar los resultados.

**Conclusiones:**

- Se puede ver como la herencia simplifica el trabajo sobretodo en la clase AtaqueEspecial que no posee metodos ya que hereda todos los que necesita de Ataque
- El uso de git es fundamental al menos en equipos que no pueden juntarse, los commits bien hechos son muy utiles y  la comunicacion tambien es muy importante en la toma de decisiones.

//para que se lean los acentos y ñ = archivo > propiedades > codificacion del archivo de texto > UTF-8.